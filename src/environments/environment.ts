// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};

export const firebaseConfig = {
  apiKey: 'AIzaSyBg7M7LNnsIR-LeeIy1pAbbNor1PXwIfyw',
  authDomain: 'testing-db916.firebaseapp.com',
  projectId: 'testing-db916',
  storageBucket: 'testing-db916.appspot.com',
  messagingSenderId: '367958790287',
  appId: '1:367958790287:web:e229bb2dfd219a7ac2e0fa',
  measurementId: 'G-R6R3T58T14'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
